// import original module declarations
import 'styled-components';

// import { CSSProp } from "styled-components";

// declare module "styled-components" {
//   export interface DefaultTheme {
//     id:String,
//     lightBg: Boolean,
//     lightText:Boolean,
//     lightTextDesc:Boolean,
//     topLine: String,
//     headline: String,
//     description: String,
//     imgStart: Boolean,
//     img: String,
//     alt:String,
//     dark: Boolean,
//     primary:Boolean,
//     darkText:Boolean,
//   }

// }

// and extend them!
declare module 'styled-components' {
  export interface MyTheme {
    borderRadius: string;
    topNavBarHeight: string;
    colors: {
      main: string;
      secondary: string;
    };
  }
}

declare module "react" {
    interface Attributes {
      css?: CSSProp;
    }
}