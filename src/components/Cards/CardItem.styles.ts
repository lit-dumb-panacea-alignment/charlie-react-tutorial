import styled, { css } from 'styled-components';

export const CardStyles = styled.div`
  a {
    text-decoration: none;
    color: black;
  }
  .main_content {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    width: 60vw;
    height: 50vh;
    background: #f8f6f6;
    border-radius: 15px;
    box-shadow: 5px 7px black;
  }
  .info {
    grid-column: 2/-1;
    display: grid;
    place-items: center;
  }
  .info h1 {
    font-family: 'Playfair Display', serif;
    margin-bottom: 0px;
  }
  .info p {
    font-family: 'Playfair Display', serif;
  }

  .container {
    display: grid;
    place-items: center;
    width: 100vw;
    height: 70vh;
    position: relative;
    top: 0vh;
    background: #f3f4f5;
  }
  .middle {
    position: absolute;
    top: 50%;
    left: 75%;
    transform: translate(-50%, -50%);
  }
  .btn {
    position: relative;
    display: block;
    color: red;
    font-size: 14px;
    font-family: 'Playfair Display', serif;
    text-decoration: none;
    margin: 30px 0;
    border: 2px solid black;
    padding: 14px 60px;
    text-transform: uppercase;
    overflow: hidden;
    transition: 1s all ease;
  }

  .btn::before {
    background: black;
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: -1;
    transition: all 0.6s ease;
  }

  .btn1::before {
    width: 0%;
    height: 100%;
  }

  .btn1:hover::before {
    width: 100%;
  }

  // .btn {
  //   width: 130px;
  //   height: 45px;
  //   border: solid;
  //   display: grid;
  //   place-items: center;
  //   border-radius: 25px;
  //   background-image: linear-gradient(to right, #53bcbd, #fec556);
  //   cursor: pointer;
  //   box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.2);
  // }
  .img {
    grid-column: 1/2;
    display: grid;
    place-items: center;
    overflow: hidden;
  }
  .card_img {
    border-radius: 15px;
  }

  @media screen and (max-width: 690px) {
    .middle {
      top: 40%;
      left: 50%;
    }
    .main_content {
      height: 70vh;
      width: 80vw;
      grid-template-columns: 100%;
      grid-template-rows: 55% 45%;
      display: grid;
    }
    .card_img {
      width: 100%;
      object-fit: contain;
    }
    .info {
      grid-column: 1/-1;
      display: grid;
      place-items: center;
      font-size: 12px;
    }
    .btn {
      height: 60px;
    }
  }
`;
