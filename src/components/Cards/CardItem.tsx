import React, { useRef, useEffect }  from 'react'
import { CardStyles } from './CardItem.styles'
import Ocean from "../../lib/Ocean.jpg"

function CardItem({header,img,subtitle, buttonURL}) {

    let card = useRef(null)

    useEffect(() =>{
         if(card.current){
    card.current.addEventListener("mousemove", (e) =>{
        let xAxis = (window.innerWidth / 2 - e.pageX) /150
        let yAxis = (window.innerHeight / 2 - e.pageY) /2400
        card.current.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg)`
                })
    
          card.current.addEventListener("mouseenter", (e) =>{
                card.current.style.transition = "none"
          })  

           card.current.addEventListener("mouseleave", (e) =>{
               card.current.style.transition = "all 0.5s ease"
               card.current.style.transform = `rotateY(0deg) rotateX(0deg)`
           })
        }
    }, [])
   


    return (
        <CardStyles>
<div className="container">
        <div className = 'main_content' ref = {card}>
            <div className="img"><img  className = "card_img" src={img} alt="gameController" /></div>
            <div className="info">
            <h1>{header}</h1>
            <p>{subtitle}</p>
           <div className="middle"><a href = {buttonURL} target = "_blank"  className="btn btn1">View project</a></div>
            </div>
        </div>
</div>
        </CardStyles>
    )
}

export default CardItem
