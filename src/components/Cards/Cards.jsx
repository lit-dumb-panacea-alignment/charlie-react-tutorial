import React from 'react'
import CardItem from "./CardItem"
import gameController from "../../lib/gameController.svg"
import Music from "../../lib/Music.jpeg"
import {Cardss} from "./Cards.styles"

import {firstCard, SecondCard ,thridCard} from "./Cards.data"
function Cards() {

   
    return (
        <Cardss>
        <div>
            <div className = "Cards_intro_text"><h1>Our lastest work</h1></div>
            <CardItem  {...firstCard} />
            <CardItem {...SecondCard} />
            <CardItem {...thridCard} />
        </div>
        </Cardss>
    )
}

export default Cards
