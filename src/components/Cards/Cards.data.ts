import Music from "../../lib/Music.jpg"
import E  from "../../lib/eCommerce.svg"
import trees from "../../lib/pexels-leah-kelley-2090645.jpg"

export const firstCard = {
    header:"Discover new Music",
    subtitle:"   Credit - Thad Duval  ",
    img: Music,
    buttonURL:"https://namesthealbum.com"
}
export const SecondCard = {
    header:"Ecommerce Magic",
    subtitle:"   Credit - Sayid Muhammad  ",
    img: E,
    buttonURL:"https://github.com/SayidMuhammad-programmer/Ecommerce-Store/tree/master"
}
export const thridCard = {
    header:"Miami",
    subtitle:"   Credit - Chucky Ohana ",
    img: trees,
    buttonURL:"https://namesthealbum.com"
}