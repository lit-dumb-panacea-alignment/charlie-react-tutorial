import styled, { css } from 'styled-components';

export const Contact = styled.div`
  background: #010101;
  height: 100vh;
  font-family: 'Playfair Display', serif;

  .load-container {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    overflow: hidden;
    z-index: 10;
    pointer-events: none;
  }
  .load-screen {
    position: relative;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 0px;
    background-color: #19bc8b;
    width: 0%;
    height: 100%;
  }

  h1 {
    color: white;
  }

  .form-inputs {
    margin-bottom: 30px;
    height: 40px;
  }
  
  form {
    height: 100vh;
    display: flex;
    margin: 0 auto;
    flex-direction: column;
    // align-items: center;
    justify-content: center;
    width: 80%;
  }

  .top_group {
    display: flex;
    width: 100%;
    justify-content: space-between;
  }
  .form-inputs {
    font-family: 'Playfair Display', serif;
    display: block;
    padding-left: 10px;
    outline: none;
    border-radius: 10px;
    height: 45px;
    width: 100%;
    border: none;
    margin-right: 50px;
  }
  .form-inputse::placeholder {
    opacity: 0.7;
  }
  .form-inputs-message {
    display: block;
    padding: 20px;
    outline: none;
    border-radius: 15px;
    height: 50vh;
    width: 100%;
    border: none;
  }
  .form-inputs-message::placeholder {
    font-size: 1.2em;
    margin: 10px;
    opacity: 0.7;
    font-family: 'Playfair Display', serif;
  }
  .btn {
    text-decoration: none;
    position: relative;
    color: #ffff;
    text-decoration: none;
    z-index: 2;
    width: 100%;
    height: 45px;
    background-image: linear-gradient(to top, #19bc8b, #19bc8b);
    border: none;
    border-radius: 10px;
    color: white;
    font-family: 'Playfair Display', serif;
    font-size: 1.1rem;
    margin-top: 30px;
    cursor: pointer;
  }
  .btn:after {
    content: '';
    top: 0;
    position: absolute;
    left: 0;
    width: 0;
    height: 100%;
    background: #4837ff;
    transition: all 0.65s;
    border-radius: 10px;
  }
  .btn:hover {
    color: #fff;
  }
  .btn:hover:after {
    width: 100%;
  }
  .google-maps {
    width: 90%;
    height: 70vh;
    position: absolute;
    top: 100vh;
    left: 5vw;
  }
  .pin-icon {
    font-size: 1.5rem;
  }
  #GMA_h4 {
    color: white;
    margin-bottom: 20px;
  }
  @media screen and (max-width: 690px) {
    .Textgroup {
      display: none;
    }
    form {
      width: 85%;
      left: 10%;
    }
  }
`;
