import mail from "../../lib/mail.gif"

export const ContactData = {

    Email:"yrnworld6@gmail.com",
    Phone: "(305)-815-1342",
    Location:"Miami, FL",
    LinkedIn: "  Lorem ipsum dolor sit amet consectetur.",
    Twitter: "    Lorem ipsum dolor sit amet consectetur.",
    Quote: "You have brains in your head. You have feet in your shoes. You can steer yourself any direction you choose. -Dr. Seuss"

}
export const location = {
  address: 'Miami.',
  lat: 25.761681,
  lng:  -80.191788,
}
export const MailObj = {
    id:"About",
    lightBg: true,
    lightText:false,
    lightTextDesc:true,
    topLine:"We read our mail!",
    headline: "Shoot us a quick email",
    description: "We are alway open to constructive criticism",
    imgStart: true,
    img: mail,
    alt:"react",
    dark: true,
    primary:true,
    darkText:true
}