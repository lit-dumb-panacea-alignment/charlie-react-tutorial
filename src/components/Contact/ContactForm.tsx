import React, { useRef, useEffect, useState } from 'react'
import { Contact } from "./Contact.styles";
import { ContactData, location } from "./ContactData"
import GoogleMapReact from 'google-map-react'
import { Icon } from '@iconify/react'
import locationIcon from '@iconify/icons-mdi/map-marker'
import gsap from 'gsap'

function ContactForm() {

  const AnyReactComponent = ({ text, lat, lng }) => <div>
    <Icon icon={locationIcon} className="pin-icon" />
    {text}
  </div>;

  let screen = React.useRef(null);
  let body = React.useRef(null);


  useEffect(() => {
    let newBody = body.current
    var tl = new TimelineMax();
    tl.to(screen.current, {
      duration: 0.5,
      width: "100%",
      left: "0%",
      ease: Power3.easeInOut,
    });

    tl.to(screen.current, {
      duration: 0.5,
      left: "100%",
      ease: Power3.easeInOut,
      delay: 0.2,
    });

    tl.set(screen.current, { left: "-100%" });

    TweenMax.to(body.current, .4, {
      css: {
        opacity: "1",
        pointerEvents: "auto",
        ease: Power4.easeInOut
      }
    }).delay(2);
    return () => {

      TweenMax.to(newBody, 1, {
        css: {
          opacity: "0",
          pointerEvents: 'none'
        }
      });
    }
  }, []);

  return (

    <Contact>
      <div className="load-container">
        <div className="load-screen" ref={screen}></div>
      </div>
      <div className="body" ref={body}>
        {/* <h1>Contact</h1> */}
        <form action="https://formspree.io/f/xjvpqnab" method="POST" id="confirmationForm">
          <div className="top_group">
            <div className="form_inputs">
              <input
                id="Name"
                type="text"
                name="Name"
                className="form-inputs  Name"
                placeholder="Name"
              />
            </div>
            <div className="form_inputs">
              <input
                id="email"
                type="Email"
                name="Email"
                className="form-inputs"
                placeholder="Email"
              />
            </div>
          </div>
          <div className="form_inputs">
            <textarea name="Message"
              form="confirmationForm"
              className="form-inputs-message"
              placeholder="Message:"
            >
            </textarea>
          </div>
          <button className="btn">Submit</button>
        </form>
      </div>
    </Contact>
  )
}

export default ContactForm
