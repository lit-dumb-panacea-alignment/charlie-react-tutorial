import React from 'react'
import { FooterStyles } from "./Footer.styles"
import { Link } from "react-router-dom"
function Footer() {
    let bot = document.getElementById("bott")

    return (
        <FooterStyles>
            <div className="main_content_footer">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#f3f4f5" fillOpacity="1" d="M0,256L48,245.3C96,235,192,213,288,208C384,203,480,213,576,224C672,235,768,245,864,218.7C960,192,1056,128,1152,122.7C1248,117,1344,171,1392,197.3L1440,224L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path>
                </svg>
                <i className="fas fa-gem"></i>
                <p>Making great things happen</p>
                {/* <div className="soical">
                    <a href="https://www.instagram.com/" target="_blank"><i className="fab fa-instagram"></i></a>
                    <a href="https://www.facebook.com/" target="_blank"><i className="fab fa-facebook-f"></i></a>
                    <a href="https://twitter.com/" target='_blank' ><i className="fab fa-twitter"></i></a>
                    <a href="https://www.linkedin.com/in/sayid-muhammad-557266186/" target='_blank'>  <i className="fab fa-linkedin-in"></i></a>
                </div> */}
                {/* <div className="bot" id="bott" onClick={() => window.scrollTo({ top: 0, behavior: `smooth` })}>
                    <div id="cursor"></div>
                </div>
                
                <div className="left_side">
                    <i className="fas fa-gem"></i>
                    <p>Making great things<br /> in Miami.</p>
                    <div className="soical">
                        <a href="https://www.instagram.com/" target="_blank"><i className="fab fa-instagram"></i></a>
                        <a href="https://www.facebook.com/" target="_blank"><i className="fab fa-facebook-f"></i></a>
                        <a href="https://twitter.com/" target='_blank' ><i className="fab fa-twitter"></i></a>
                        <a href="https://www.linkedin.com/in/sayid-muhammad-557266186/" target='_blank'>  <i className="fab fa-linkedin-in"></i></a>
                    </div>
                </div>

                <div className="right_side">
                    <div className="right_content">
                        <p>Get in touch</p>
                        <h4>TheNextJ.J.E@gmail.com</h4>
                        <h4>(305)-851-1342</h4>
                        <div className="other">
                            <div className="btn" onClick={() => window.scrollTo({ top: 0, behavior: `smooth` })}><Link to="/contact">Lets Talk</Link></div>
                            <h5 className="address">201 S Biscayne Blvd, Miami, FL 33132</h5>
                        </div>
                    </div>
                </div> */}
            </div>
        </FooterStyles>
    )
}

export default Footer
