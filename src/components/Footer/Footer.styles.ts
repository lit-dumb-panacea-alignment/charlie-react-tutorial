import styled, { css } from 'styled-components';

export const FooterStyles = styled.div`
bottom:0;
.bot{
    position: absolute;
    width:100%;
    height:15vh;
    bottom:0vh;
    cursor: pointer;
}
a{
    color:white;
}
.main_content_footer{
    background:black;
    width:100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}
.main_content_footer p{
color:#fff;
font-size:25px;
font-family: 'Playfair Display', serif;
margin-bottom: 15vh;
}
}
.left_side{
    position: fixed;
    left:20%;
}
.right_side{
    position: fixed;
    right:20%;
    height: 50vh;
    width:300px;
    display:grid:
    place-items:center;
}
.fas{
    margin-bottom: 70px;
    font-size:40px;
    color:#fff;
}
.right_content p {
    font-size:21px;
    margin-bottom: 70px;
    
}
.right_content h4 {
    color:#bb3820;
    margin-bottom: 15px;
}
.soical{
        display:grid;
        grid-template-columns:repeat(4, 35px) ;
        color:#fff;
        margin-bottom: 15px;
}
.other{
    margin-top:20px;
    display:grid;
    place-items:center;
}
.btn{
    cursor: pointer;
    border:solid white;
    width:110px;
    height:45px;
    color:white;
    display:grid;
    place-items:center;

}
.address{
    color : #343332;
    margin-top:60px;
}

@media screen and (max-width: 690px) {
    .right_content {
        display:grid;
        place-items:center;
    }
.main_content_footer{
// height:100vh;
background:black;
width:100%;
overflow: hidden;
}
.right_content p{
   cursor: pointer;
    border:solid white;
    width:210px;
    height:45px;
    color:white;
    display:grid;
    place-items:center;
     position:relative ;
    
}
.right_content h5{
    display:none;
    
}
.btn{
display:none;
}


.left_side{
    display:flex;
    flex-direction: column;
    align-items: center;
    position: sticky;
    left:50%;
    overflow: hidden;

}
.soical{
    display:flex;
     justify-content: space-between;
     overflow: hidden;
}
}
`;
