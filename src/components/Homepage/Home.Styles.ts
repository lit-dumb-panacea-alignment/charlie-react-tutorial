import styled, { css } from 'styled-components';

export const HomeStyles = styled.div`


.main_content{
    width:100vw;
    height:100vh;
   // position: fixed;  : position: relative;
    display:grid;
    place-items: center;
    top:0;
    z-index: -1;
}
.mid_text{
    color: white;
    font-size:4rem;
}
.a_tag{
text-decoration: none;
 color:white;
}
.mid_content h3{
    color: white;
    text-align: center;
    margin-bottom:20px;
    font-size:2rem;
}
.human{
    position: absolute;
    top:60vh;
    right:0vw;
    transform: scaleX(-1);
}
.mid_content {
    color: white;
    display:grid;
    place-items:center;
    margin-bottom:20px;
}
.btn{
    width:110px;
    height:45px;
    display:grid;
    place-items:center;
    border: solid;
    margin-bottom:20px;
    cursor: pointer;
}
.tri{
    clip-path: polygon(50% 0%, 0% 100%, 100% 100%);
    background-color:white;
    width:30px;
    height:30px;
    margin-bottom:20px;
    
}
.cir{
    position:absolute;
    clip-path: circle(44.6% at 49% 51%);
    background-image: linear-gradient(to top, #6649b8 0% , #ffb199 100% );
    width:80px;
    height:80px;
    left:20%;
    top:210px;
    z-index: -1;
}
.load-container {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  overflow: hidden;
  z-index: 10;
  pointer-events: none;
}
.load-screen {
  position: relative;
  padding-top: 0px;
  padding-left: 0px;
  padding-right: 0px;
  background-color: #19bc8b;
  width: 0%;
  height: 100%;
}

@media screen and (max-width: 690px) {
.main_content{
      overflow: hidden;

}
.mid_text{
    font-size:55px;
}
.mid_content h3{
    font-size:14px;
}
.btn{
    width:90px;
    height:35px;
    font-size:15px;
}
.mid_content {
}
}

`;