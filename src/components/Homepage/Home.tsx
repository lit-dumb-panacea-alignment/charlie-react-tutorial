import React, { useRef, useEffect, useState } from 'react'
import { HomeStyles } from "./Home.Styles"
import Human from "../../lib/Humaaans.png"
import { TweenMax, TimelineMax, Power3, Power4 } from "gsap";
import gsap from "gsap"
import { useHistory } from 'react-router-dom'
import { ScrollTrigger } from 'gsap/ScrollTrigger';


function Home() {
  gsap.registerPlugin(ScrollTrigger);
  let history = useHistory()
  let Height = window.innerHeight;

  let tri = React.useRef<HTMLDivElement>(null);
  let screen = React.useRef(null);
  let body = React.useRef(null);
  let Humann = React.useRef(null);

  useEffect(() => {


    gsap.fromTo(Humann.current, {
      autoAlpha: 1,
    }, {
      duration: 1,
      autoAlpha: 0,
      ease: "none",
      scrollTrigger: {
        toggleActions: 'play none none reverse'
      }
    });


    gsap.timeline().to(tri.current, { y: 10, delay: 7.2, duration: 1 }).to(tri.current, { y: -10, duration: 1 })

  }, [])



  useEffect(() => {


    var tl = new TimelineMax();
    tl.to(screen.current, {
      duration: 0.5,
      width: "100%",
      left: "0%",
      ease: Power3.easeInOut,
    });

    tl.to(screen.current, {
      duration: 0.5,
      left: "100%",
      ease: Power3.easeInOut,
      delay: 0.2,
    });

    tl.set(screen.current, { left: "-100%" });

    TweenMax.to(body.current, .3, {
      css: {
        opacity: "1",
        pointerEvents: "auto",
        ease: Power4.easeInOut
      }
    }).delay(2);
    return () => {
      let newBody = body.current
      TweenMax.to(newBody, 1, {
        css: {
          opacity: "0",
          pointerEvents: 'none'
        }
      });
    }



  }, []);

  return (
    <HomeStyles>
      <div className="load-container">
        <div className="load-screen" ref={screen}></div>
      </div>

      <div className="shapes">
        <div className="cir"></div>
        <div className="human" ref={Humann}><img src={Human} alt="Human" width="250px" /></div>
      </div>

      <div ref={body} className="main_content">
        <div className="mid_content">
          <div className="mid_text">Alignment Agency</div>
          <h3>A team of software engineers and developers</h3>
          <div className="btn" onClick={() => window.scrollTo({ top: Height, behavior: 'smooth' })}>
            Explore
          </div>
          <div className="tri" ref={tri}></div>
        </div>
      </div>

    </HomeStyles>
  )
}

export default Home
