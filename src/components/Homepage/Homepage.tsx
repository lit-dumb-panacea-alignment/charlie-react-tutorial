import React from 'react'
import Home from "./Home"
import { homeObjOne, homeObjTwo, homeObjThree } from '../Info/Data'
import InfoSection from "../Info/InfoIndex"

function Homepage() {
    return (
        <div>
            <Home />
            <InfoSection  {...homeObjOne} />
            <InfoSection  {...homeObjTwo} />
            <InfoSection  {...homeObjThree} />
        </div>
    )
}

export default Homepage
