import * as React from "react";
import { AiOutlineHome } from "react-icons/ai";
import {
	Toolbar,
	Navigation,
	Links,
	Items,
	ListItem,
	Spacer,
	LogoBlock
} from './styles'

import DrawerToggleButton from '../SideDrawer/DrawerToggleButton'

import { useHistory } from "react-router-dom";

import { DefaultTheme } from 'styled-components';

interface NavProps {
	onClick: () => void,
	theme: DefaultTheme
}

export function Nav(props: NavProps) {
	let history = useHistory()

	const {
		theme
	} = props

	return (
		<Toolbar>
			<Navigation>
				<div>
					<DrawerToggleButton onClick={props.onClick} />
				</div>
				<LogoBlock>
					<Links onClick={() => {
						history.push("/")
					}}>
						<AiOutlineHome />
					</Links>
				</LogoBlock>
				<Spacer />
				<div>
					<Items>
						<ListItem>
							<Links onClick={() => {
								history.push("/");
							}}>
								Home
							</Links>
						</ListItem>
						<ListItem>
							<Links onClick={() => {
								history.push("/about");
							}}>
								About
							</Links>
						</ListItem>
						<ListItem>
							<Links onClick={() => {
								history.push("/contact");
							}}>
								Contact
							</Links>
						</ListItem>
					</Items>
				</div>
			</Navigation>
		</Toolbar >
	)
}