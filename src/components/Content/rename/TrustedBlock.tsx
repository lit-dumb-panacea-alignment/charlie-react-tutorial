import React , {useRef } from 'react'
import {Tb} from "./TB.styles"
import gsap from 'gsap'
import rocket from "../../../lib/day20-rocket.png"


function TrustedBlock() {

    let Trustb = React.useRef<HTMLDivElement>(null);

    // gsap.from(Trustb, {
    //     scrollTrigger: Trustb,

    // })

    return (
        <Tb>
        <div className = "main_content" >
            <h4>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, pariatur!</h4>
            <img src={rocket} alt="" width = "200px"/>
        </div>
    
       
        </Tb>
    )
}

export default TrustedBlock
