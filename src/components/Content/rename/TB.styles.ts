import styled, { css } from 'styled-components';

export const Tb = styled.div`
  .main_content {
    position: absolute;
    top: 100vh;
    width: 100vw;
    height: 270px;
    background: #554d84;
    opacity: 1;
    display: grid;
    place-items: center;
    z-index: -2;
  }
  .main_content h4 {
    font-family: 'Roboto', sans-serif;
    color: white;
    font-weight: bold;
  }
  .main_content img {
    position: absolute;
    z-index: -1;
  }
`;
