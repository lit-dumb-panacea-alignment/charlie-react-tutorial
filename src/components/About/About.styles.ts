import styled, { css } from 'styled-components';

export const Button = styled.div`
  position: absolute;
  top: 371vh;
  left: 67%;
  transform: translate(-50%, -50%);
  cursor: pointer;

  @media screen and (max-width: 690px) {
    top: 380vh;
    left: 50%;
  }

  a {
    text-decoration: none;
    color: white;
  }

  .btn2::before {
    width: 100%;
    height: 0%;
  }
  .btn2:hover::before {
    height: 100%;
  }

  .btn {
    position: relative;
    display: block;
    color: black;
    font-size: 14px;
    font-family: 'Playfair Display', serif;
    text-decoration: none;
    margin: 30px 0;
    border: 1px solid #1e1e24;
    padding: 14px 60px;
    text-transform: uppercase;
    overflow: hidden;
    transition: 1s all ease;
  }
  .btn::before {
    background: #6649b8;
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: -1;
    transition: all 0.6s ease;
  }
`;

export const Aboutstyles = styled.div`
  .resume_loader {
  }
  .main_container {
    display: grid;
    place-items: center;
    width: 100vw;
    height: 100vh;
    color: white;
    position: relative;
  }

  .txtbox {
    // display: grid;
    // width: 45vw;
    // height: 25vh;
    // color: white;
    // position: absolute;
    // top: 25%;
    // left: 25%;
    // z-index: 1;
    font-size: 1.6rem;
    // place-items: center;
  }

  .load-container {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    overflow: hidden;
    z-index: 10;
    pointer-events: none;
  }
  .load-screen {
    position: relative;
    padding-top: 0px;
    padding-left: 0px;
    padding-right: 0px;
    background-color: #19bc8b;
    width: 0%;
    height: 100%;
  }
  .btn {
    position: absolute;
    top: 70%;
    border: solid white;
    width: 110px;
    height: 45px;
    display: grid;
    place-items: center;
  }
  .btn a {
    text-decoration: none;
    color: white;
  }
  .img_div h1 {
    color: white;
    font-size: 3rem;
    font-family: 'Playfair Display', serif;
  }
  .img_div {
    display: grid;
    place-items: center;
  }
  .night {
    position: absolute; /* position: relative; */
    width: 100%;
    height: 100%;
    left: -550px;
    top: -300px;
    transform: rotateZ(40deg);
  }

  @media screen and (max-width: 690px) {
    .img_div h1 {
      font-size: 2rem;
    }
    .img {
      width: 100%;
      min-width: 400px;
    }
    .img_div {
      width: 80vw;
      display: grid;
      place-items: center;
      position: fixed;
      top: 20vh;
      left: 5vh;
    }
  } /*End of media */

  .img {
    width: 60%;
  }

  .star {
    position: absolute;
    left: 20%;
    top: 0;
    height: 4px;
    background: linear-gradient(-45deg, #fff, rgba(0, 0, 255, 0));
    border-radius: 999px;
    filter: drop-shadow(0 0 6px #699bff);
    animation: tail 3s ease-in-out infinite, falling 3s ease-in-out infinite;
  }

  @keyframes tail {
    0% {
      width: 0;
    }
    30% {
      width: 100px;
    }
    100% {
      width: 0;
    }
  }

  @keyframes falling {
    0% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(300px);
    }
  }

  .star::before,
  .star::after {
    content: '';
    position: absolute;
    top: calc(50% - 2px);
    right: 0;
    height: 4px;
    background: linear-gradient(
      -45deg,
      rgba(0, 0, 255, 0),
      #5f91ff,
      rgba(0, 0, 255, 0)
    );
    border-radius: 100%;
    transform: translateX(50%) rotateZ(45deg);
    animation: shining 3s ease-in-out infinite;
  }

  @keyframes shining {
    0% {
      width: 0;
    }
    50% {
      width: 30px;
    }
    100% {
      width: 0;
    }
  }

  .star::after {
    transform: translateX(50%) rotateZ(-45deg);
  }

  .star:nth-child(1) {
    top: calc(50% - 100px);
    left: calc(50% - 250px);
    animation-delay: 1s;
  }

  .star:nth-child(1)::before,
  .star:nth-child(1)::after {
    animation-delay: 1s;
  }

  .star:nth-child(2) {
    top: calc(50% - 50px);
    left: calc(50% - 200px);
    animation-delay: 1.2s;
  }

  .star:nth-child(2)::before,
  .star:nth-child(2)::after {
    animation-delay: 1.2s;
  }

  .star:nth-child(3) {
    top: calc(50% - 0px);
    left: calc(50% - 150px);
    animation-delay: 1.4s;
  }

  .star:nth-child(3)::before,
  .star:nth-child(3)::after {
    animation-delay: 1.4s;
  }

  .star:nth-child(4) {
    top: calc(50% - -50px);
    left: calc(50% - 200px);
    animation-delay: 1.6s;
  }

  .star:nth-child(4)::before,
  .star:nth-child(4)::after {
    animation-delay: 1.6s;
  }

  .star:nth-child(5) {
    top: calc(50% - -100px);
    left: calc(50% - 250px);
    animation-delay: 1.8s;
  }

  .star:nth-child(5)::before,
  .star:nth-child(5)::after {
    animation-delay: 1.8s;
  }
`;
