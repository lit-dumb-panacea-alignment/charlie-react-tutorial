import React, { useEffect, useRef } from 'react'
import { Aboutstyles } from "./About.styles"
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger';

import { useHistory } from 'react-router-dom'

function About() {
  gsap.registerPlugin(ScrollTrigger);
  let About = useRef(null)
  let screen = React.useRef(null);
  let body = React.useRef(null);

  let history = useHistory()

  useEffect(() => {
    gsap.fromTo(About.current, {
      y: 0,
      autoAlpha: 1
    }, {
      y: -100,
      duration: 1,
      ease: "none",
      autoAlpha: 0,
      scrollTrigger: {
        trigger: About.current,
        start: 'top-=100 top',
        toggleActions: 'play none none reverse'
      }
    });

    var tl = new TimelineMax();
    tl.to(screen.current, {
      duration: 0.5,
      width: "100%",
      left: "0%",
      ease: Power3.easeInOut,
    });

    tl.to(screen.current, {
      duration: 0.5,
      left: "100%",
      ease: Power3.easeInOut,
      delay: 0.3,
    });

    tl.set(screen.current, { left: "-100%" });

    TweenMax.to(body.current, .3, {
      css: {
        opacity: "1",
        pointerEvents: "auto",
        ease: Power4.easeInOut
      }
    }).delay(2);
    return () => {
      let newBody = body.current
      TweenMax.to(newBody, 1, {
        css: {
          opacity: "0",
          pointerEvents: 'none'
        }
      });
    }
  }, []);


  return (
    <Aboutstyles>
      <div>
        <div className="load-container">
          <div className="load-screen" ref={screen}></div>
        </div>

        <div ref={body} className="main_container">
          <div className="txtbox">
            We are a full-service solutions agency. What sets us apart is our complete
            focus on results. Establishing the right connection with our clients is paramount
            to our mision. Working with us will be a memorable and meaningful experience.
          </div>
        </div>
      </div>
    </Aboutstyles>
  )
}

export default About
