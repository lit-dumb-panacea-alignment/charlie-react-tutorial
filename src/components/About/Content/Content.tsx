import React, { useRef, useEffect } from 'react'
import { Contentstyles } from "./Content.styles"
import GOAT from "../../../lib/GOAT.jpg"
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger';
// import anime from 'animejs/lib/anime.es.js';

function Content() {
    gsap.registerPlugin(ScrollTrigger);


    let wrapper = useRef(null)
    let box = useRef(null)
    let textbox = useRef(null)



    useEffect(() => {

        gsap.to(wrapper.current, 3, {
            scrollTrigger: {
                trigger: wrapper.current,
                start: "top bottom - 300px",
            },
            top: "-100%",
            ease: Expo.easeInOut,
            delay: 3.6
        });

        gsap.to(box.current, 2.4, {
            scrollTrigger: {
                trigger: wrapper.current,
                start: "top bottom - 300px",
            },
            y: "-100%",
            ease: Expo.easeInOut,
            delay: 3.8,
        });
    }, [])




    return (
        <Contentstyles>

        </Contentstyles>
    )
}

export default Content
