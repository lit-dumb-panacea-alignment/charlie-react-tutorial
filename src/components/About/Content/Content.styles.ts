import styled, { css } from 'styled-components';

export const Contentstyles = styled.div`
  .main_container {
    height: 100vh;
    width: 100vw;
    // background:white;
    display: grid;
    grid-template-columns: 1fr 1fr;
    position: relative;
  }
  .container {
    grid-column: 2/-1;
  }
  .wrapper-img {
    overflow: hidden;
    position: relative;
    top: 10% !important;
    // left: 50%;
    transform: translate(-50%, 0%);
    width: 70%;
    //   height: 90vh;
  }
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  .box {
    background: rgb(26, 3, 3);
    opacity: 1;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    z-index: 2;
  }
  .wrapper {
    position: fixed;
    width: 100%;
    height: 100vh;
    background: #000;
    z-index: 2;
  }
  .txtbox {
    display: grid;
    border: solid;
    width: 45vw;
    height: 25vh;
    color: grey;
    position: absolute;
    top: 50%;
    left: 25%;
    z-index: 1;
    font-size: 1.3rem;
    place-items: center;
  }
  .header {
    position: absolute;
    top: 40%;
    left: 28%;
    transform: translate(-50%, -50%);
    color: grey;
  }
  .header h1 {
    font-size: 90px;
    text-transform: uppercase;
    margin: 0;
    mix-blend-mode: overlay;
  }

  .header-1 .letter {
    display: inline-block;
    line-height: 1em;
  }
  .header-2 .letter {
    display: inline-block;
    line-height: 1em;
  }
  @media screen and (max-width: 690px) {
    .header {
      display: none;
    }
    .main_container {
      grid-template-columns: 2%;
    }
    .txtbox {
      top: 40%;
      height: 45vh;
      border: none;
    }
    .container {
      position: absolute;
      top: -20vh;
    }
  }
`;
