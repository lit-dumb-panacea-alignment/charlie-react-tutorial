import styled, { css } from 'styled-components';

export const Tb = styled.div`
  font-family: 'Playfair Display', serif;

  .group-project {
    //background:red;
    position: absolute;
    height: 40%;
    display: grid;
    place-items: center;
    left: -280px;
    top: -90px;
    display: inline;
  }
  .memeber {
    border: solid;
    height: 150px;
    width: 150px;
    border-radius: 100%;
    // background:green;
  }
  .img {
    width: 100%;
    border-radius: 100%;
    height: 100%;
  }

  .middle {
    position: absolute;
    top: 80%;
    right: 17%;
  }
  .btn {
    position: relative;
    display: block;
    color: white;
    font-size: 0.8rem;
    font-family: 'Playfair Display', serif;
    text-decoration: none;
    padding: 14px 60px;
    text-transform: uppercase;
    overflow: hidden;
    transition: 1s all ease;
  }
  .btn::before {
    background: #5ba8;
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: -1;
    transition: all 0.6s ease;
  }

  .btn4::before {
    width: 100%;
    height: 0%;
    transform: translate(-50%, -50%) rotate(-45deg);
  }
  .btn4:hover::before {
    height: 380%;
  }
  .main_content {
    position: relative;
    top: 0;
    width: 100vw;
    height: 100vh;
    display: grid;
    place-items: center;
    overflow: hidden;
  }
  .t {
    color: #f2eae4;
    position: absolute;
    font-size: 28rem;
    right: 20%;
    opacity: 0.3;
    top: 10vh;
    place-items: center;
    width: 50%;
    right: 0;
  }
  .trust_txt {
    color: black;
    position: absolute;
    display: grid;
    place-items: center;
    left: 0;
    top: 0;
    background: #f0e8e0;
    height: 100vh;
    width: 50vw;
  }
  .trust_txt h1 {
    //margin-bottom: 0px;
    z-index: 200;
  }
  .trust_txt p {
    //margin-bottom: 20px;
    font-size: 18px;
  }

  @media screen and (max-width: 690px) {
    .a {
      z-index: -1;
    }
    .trust_txt {
      place-items: center;
      position: sticky;
      width: 100%;
    }
    .middle {
      z-index: 10;
      top: 65%;
      right: 30%;
    }
  }
`;
