import React, { useRef, useEffect } from 'react';
import {Tb} from "./TB.styles"
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import oldMan from "../../lib/oldMan.svg"
import Doodle from "../../lib/Doodles.png"

function TrustedBlock() {
  gsap.registerPlugin(ScrollTrigger);
  let t =  useRef(null)
  let Text =  useRef(null)
  let jumper = useRef(null)

useEffect(() =>{

  gsap.fromTo(jumper.current, {
    x:0,
    y:0,
  },{
      x:280,
      y:90,
      delay:0.5,
      duration: 3, 
        scrollTrigger: {
          trigger: t.current,
          start: 'top center+=100',
          toggleActions: 'play none none reverse'
        }
  })
      
      gsap.fromTo(t.current, {
        autoAlpha: 0,
        x:200,
      }, {
        x:0,
        duration: 1, 
        autoAlpha: 0.5,
        ease: "none",
        
        scrollTrigger: {
          trigger: t.current,
          start: 'top center+=100',
          toggleActions: 'play none none reverse'
        }
      });

      gsap.fromTo(Text.current, {
        x:-600,
      duration: 1,
    },
      {
        x:0,
        duration: 1,
      scrollTrigger:{
          trigger:Text.current,
          start: 'top center+=100',
          toggleActions: 'play none none reverse'
      }
    })

},[])
    

    return (
        <Tb>
        <div className = "main_content" >
          <div className="t" ref = {t}>
            <img src ={oldMan} alt = ""  className = "oldman"/>
              <strong>T</strong>
        </div>
        <div className="middle">
        <a href="" className="btn btn4">Follow me!</a>
        </div>
          <div className="trust_txt" ref = {Text}>
            <h1>Hey</h1>

            <p> My name is Sayid<br/> I designed this project to showcase my dev skills and give thanks to, two great<br/> mentors.  Im located in Mimai 
            Florida and im actively searching for oppunity. If your a recuiter , fellow software engineers or my mom, welcome!  <br/> Feel free to critique my work and give me some feed back<br/>
             </p>
             <div className="group-project" ref = {jumper}>
                <div className="memeber">
                  <img src = {Doodle} alt = "sayid" width = "300px"/>
                </div>
       
             </div>

              <p>
                  <i>You can dream, create, design and build the most wonderful place in the world…
                      <br/>but it requires people to make the dream a reality.</i></p>
          </div>
        </div>
        </Tb>
    )
}

export default TrustedBlock
