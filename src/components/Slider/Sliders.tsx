import React, { useRef, useEffect } from 'react'
// import "./styles.css"

import gsap from 'gsap'
import { SliderStyles } from './Sliders.styles';

function Sliders() {
    //  Grabbing my slider DOM elements
    let Slider = React.useRef<HTMLDivElement>(null);
    let Slider_1 = React.useRef<HTMLDivElement>(null);
    let span1 = React.useRef<HTMLSpanElement>(null);

    //  Grabbing the  height of  the window to animate slider
    const viewheight = window.innerHeight

    useEffect(() => {
        //     Creating a gsap timeline
        const tl = gsap.timeline({ defaults: { duration: 2.5, delay: 4.5 } })

        tl.to(Slider.current, {
            y: -viewheight * 2,
        })
        gsap.from(span1.current, {
            y: "0%",
            delay: 5,
            duration: 3.5,
            opacity: 0,
        })
        gsap.to(Slider_1.current, {
            x: "100%",
            delay: 7,
        })
        // gsap.to(Slider_1.current, {
        //     x: "100%",
        //     delay: 7,
        // })




    }, [])

    return (
        <SliderStyles>
            <div className="slider" ref={Slider}>
                <div className="wrapper">
                    <div className="text-1 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-2 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-3 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-4 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-5 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-6 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-7 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-8 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-9 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-10 text">Lit Dumb Panacea Alignment</div>
                    <div className="text-11 text">Lit Dumb Panacea Alignment</div>
                </div>
            </div>
            <div className="slider_1" ref={Slider_1}>

                <div className="slide_1_wrapper">
                    <h1 className="hide">
                        <span className="text2" ref={span1}> Friendly Neighborhood Webdesigners</span>
                    </h1>
                </div>
            </div>
        </SliderStyles>
    )

}

export default Sliders
