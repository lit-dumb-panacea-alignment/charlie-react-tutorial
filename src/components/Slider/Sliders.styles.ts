import styled, { css } from 'styled-components';

export const SliderStyles = styled.div`
  .slider_1 {
    width: 100vw;
    height: 90vh;
    background-color: rgb(255, 255, 255);
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .text2 {
    font-weight: bold;
    font-family: 'Monument Extended', Arial, Helvetica, sans-serif;
    font-size: 3rem;
  }

  .hide span {
    transform: translateY(-100%);
    display: inline-block;
  }

  .slider {
    height: 100vh;
    width: 100vw;
    background-color: #000000;
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0 auto;
  }

  .wrapper {
    color: black;
    position: absolute;
  }

  .text {
    font-family: 'Monument Extended', Arial, Helvetica, sans-serif;
    font-weight: lighter;
    font-size: 54px;
  }

  .text-1,
  .text-2,
  .text-3,
  .text-4,
  .text-5,
  .text-6,
  .text-7,
  .text-8,
  .text-9,
  .text-10,
  .text-11 {
    color: rgba(0, 0, 0, 0);
    -webkit-text-stroke: 1px rgb(255, 255, 255);
  }

  @keyframes blink {
    0% {
      opacity: 0;
    }
    1% {
      opacity: 100%;
    }
    99% {
      opacity: 100%;
    }
    100% {
      opacity: 0%;
    }
  }

  .text-1 {
    animation: blink 0.8s linear 0.9s, blink 0.8s linear 2s;
    opacity: 0;
  }

  .text-2 {
    animation: blink 0.8s linear 0.8s, blink 0.8s linear 2.1s;
    opacity: 0;
  }

  .text-3 {
    animation: blink 0.8s linear 0.7s, blink 0.8s linear 2.2s;
    opacity: 0;
  }
  .text-4 {
    animation: blink 0.8s linear 0.6s, blink 0.8s linear 2.3s;
    opacity: 0;
  }
  .text-5 {
    animation: blink 0.8s linear 0.5s, blink 0.8s linear 2.4s;
    opacity: 0;
  }
  .text-6 {
    animation: blink 0.8s linear 0.4s, blink 0.8s linear 2.5s,
      slide-out 1s linear 3.2s;
    opacity: 0;
  }
  .text-7 {
    animation: blink 0.8s linear 0.5s, blink 0.8s linear 2.4s;
    opacity: 0;
  }

  .text-8 {
    animation: blink 0.8s linear 0.6s, blink 0.8s linear 2.3s;
    opacity: 0;
  }
  .text-9 {
    animation: blink 0.8s linear 0.7s, blink 0.8s linear 2.2s;
    opacity: 0;
  }
  .text-10 {
    animation: blink 0.8s linear 0.8s, blink 0.8s linear 2.1s;
    opacity: 0;
  }
  .text-11 {
    animation: blink 0.8s linear 0.7s, blink 0.8s linear 2s;
    opacity: 0;
  }

  @keyframes slide-out {
    0% {
      opacity: 0;
    }
    1% {
      opacity: 100%;
    }
    19% {
      opacity: 100%;
    }
    20% {
      opacity: 0;
    }
    39% {
      opacity: 0;
    }
    40% {
      opacity: 100%;
    }
    59% {
      opacity: 100%;
    }
    60% {
      opacity: 0;
    }
    79% {
      opacity: 0;
    }
    80% {
      opacity: 100%;
    }
    100% {
      opacity: 100%;
    }
  }
`;
