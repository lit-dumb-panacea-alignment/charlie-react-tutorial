import * as React from "react";

import {
  List,
  Nav,
  ListItem
} from './styles'


import { DefaultTheme } from 'styled-components'
import { useHistory } from "react-router-dom";

interface Props {
  show: boolean,
  closeDrawer: () => void,
  theme: DefaultTheme
}

export function SideDrawer(props: Props) {
  const history = useHistory();

  const {
    show,
    theme
  } = props

  return (
    <Nav show={show}>
      <List>

        <ListItem onClick={() => {
          props.closeDrawer()
          history.push("/");
        }}>
          Home
        </ListItem>

        <ListItem onClick={() => {
          props.closeDrawer()
          history.push("/about")
        }}>
          About
        </ListItem>

        <ListItem onClick={() => {
          props.closeDrawer()
          history.push("/contact")
        }}>
          Contact
        </ListItem>

      </List>
    </Nav>
  )
}