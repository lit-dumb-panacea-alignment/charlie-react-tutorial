import React , {useRef} from 'react'
import {ImageSlideStyles} from "./ImageSlide.styles"
import react from "../../../lib/react.png"
import Nextjs from "../../../lib/Nextjs.png"
import hcj from "../../../lib/hcj.png"
import Design from "../../../lib/Design.png"
import success from "../../../lib/success.png"

function ImageSlide() {

        var slides = document.querySelectorAll('.slide');
    var btns = document.querySelectorAll('.btn');
    let currentSlide = 1;

    // Javascript for image slider manual navigation
    var manualNav = function(manual){
      slides.forEach((slide) => {
        slide.classList.remove('active');

        btns.forEach((btn) => {
          btn.classList.remove('active');
        });
      });

      slides[manual].classList.add('active');
      btns[manual].classList.add('active');
    }

    btns.forEach((btn, i) => {
      btn.addEventListener("click", () => {
        manualNav(i);
        currentSlide = i;
      });
    });

  //  Javascript for image slider autoplay navigation
    var repeat = function(activeClass){
      let active = document.getElementsByClassName('active');
      let i = 1;

      var repeater = () => {
        setTimeout(function(){
          [...active].forEach((activeSlide) => {
            activeSlide.classList.remove('active');
          });

        slides[i].classList.add('active');
        btns[i].classList.add('active');
        i++;

        if(slides.length == i){
          i = 0;
        }
        if(i >= slides.length){
          return;
        }
        repeater();
      }, 5000);
      }
      repeater();
    }






    return (
        <ImageSlideStyles>
            <div className="main_container">
        <div>
            <div className="img-slider">
      <div className="slide active">
        <img src={react} alt="react" height="250px" />
        <div className="info">
          {/* <h2>Slide 01</h2> */}
          
        </div>
      </div>
      <div className="slide">
        <img src={Nextjs} alt="react" height="250px" />
        <div className="info">
          {/* <h2>Slide 02</h2> */}
          
        </div>
      </div>
      <div className="slide">
        <img src={hcj} alt="react" height="250px" />
        <div className="info">
          {/* <h2>Slide 03</h2> */}
          
        </div>
      </div>
      <div className="slide">
        <img src={Design} alt="react" height="250px" />
        <div className="info">
          {/* <h2>Slide 04</h2> */}
          
        </div>
      </div>
      <div className="slide">
        <img src={success} alt="react" height="250px" />
        <div className="info">
          {/* <h2>Slide 05</h2> */}
        </div>
      </div>
      <div className="navigation">
        <div className="btn active"></div>
        <div className="btn"></div>
        <div className="btn"></div>
        <div className="btn"></div>
        <div className="btn"></div>
      </div>
    </div>
        </div>
        </div>
        </ImageSlideStyles>
    )
}

export default ImageSlide
