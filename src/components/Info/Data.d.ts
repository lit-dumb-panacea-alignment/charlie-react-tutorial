import { CSSProp } from "styled-components";

export interface HomeSection {
  id:String,
  lightBg: Boolean,
  lightText:Boolean,
  lightTextDesc:Boolean,
  topLine: String,
  headline: String,
  description: String,
  imgStart: Boolean,
  img: String,
  alt:String,
  dark: Boolean,
  primary:Boolean,
  darkText:Boolean,
}
