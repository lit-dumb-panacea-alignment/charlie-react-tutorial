import About from "../About/About";
import Girl from "../../lib/Girl.gif"
import Code from "../../lib/code.gif"
import Website from "../../lib/ContentCreator.svg"
import Portal from "../../lib/Astro.gif"
import Build from "../../lib/Building.jpg"
import resume from "../../lib/resume.svg"
export const homeObjOne = {
    id:"About",
    lightBg: false,
    lightText:true,
    lightTextDesc:true,
    topLine:"Value",
    headline: "Where can we find skilled software engineers and developers? 🤔",
    description: " Allow us to introduce ourselves 😏",
    imgStart: false,
    img: Website,
    alt:"react",
    dark: true,
    primary:true,
    darkText:false
}
export const homeObjTwo = {
    id:"About",
    lightBg: true,
    lightText:false,
    lightTextDesc:false,
    topLine:"industry professionals",
    headline: "If you can dream it, we are here to listen.",
    description: "Learning is probably our strongest skill. We are forever students.",
    imgStart: true,
    img: Girl,
    alt:"react",
    dark: true,
    primary:true,
    darkText:true
}
export const AboutpageObj = {
    id:"About",
    lightBg: true,
    lightText:false,
    lightTextDesc:false,
    topLine:"My Resume ",
    headline: "Im kind of a big deal  🤫",
    description: "Diligent, driven, and looking to leverage programming and problem-solving skills in the software engineering. Eager to further develop my skills an work on real world projects.",
    imgStart: true,
    img: resume,
    alt:"resume",
    dark: true,
    primary:true,
    darkText:true
}
export const homeObjThree = {
    id:"About",
    lightBg: false,
    lightText:true,
    lightTextDesc:true,
    topLine:"Creative Design",
    headline: "We're designers, directors, strategists, and awkward dancers.",
    description: "We have decades of experience in the tech space and creative industry, producing exciting experiences for brands that are effective.",
    imgStart: false,
    img: Portal,
    alt:"react",
    dark: true,
    primary:true,
    darkText:false
}
export const homeObjFour = {
    id:"About",
    lightBg: false,
    lightText:true,
    lightTextDesc:true,
    topLine:"Creative Design",
    headline: "Lorem ipsum dolor sit amet consectetur.",
    description: " Lorem ipsum dolor sit amet consectetur adipisicing elit. At, corrupti?",
    imgStart: false,
    img: Portal,
    alt:"react",
    dark: true,
    primary:true,
    darkText:false
}


