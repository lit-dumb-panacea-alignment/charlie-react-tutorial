import * as React from 'react';
import { DefaultTheme, ThemeProvider } from 'styled-components';

import Homepage from "./components/Homepage/Homepage"

import { Nav } from "./components/Nav"
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import Aboutpage from "./components/About/Aboutpage"
import Footer from "./components/Footer/Footer"
import Contactpage from "./components/Contact/Contactpage"
import { SideDrawer } from './components/SideDrawer';
import { Backdrop } from './components/Backdrop';

export const myTheme: DefaultTheme = {
  borderRadius: '5px',
  topNavBarHeight: '80px',
  colors: {
    main: "purple",
    secondary: "mediumseagreen",
  }
};

function App() {

  const [open, setOpen] = React.useState(false)

  return (
    <ThemeProvider theme={myTheme}>
      <Router>
        <Nav
          onClick={() => setOpen(!open)}
          theme={myTheme}
        />
        <SideDrawer
          show={open}
          theme={myTheme}
          closeDrawer={() => setOpen(false)} />
        {
          open &&
          <Backdrop onClick={() => setOpen(false)} />
        }

        <main
          style={{
            height: 'calc(100vh)'
          }}
        >
          <Switch>
            <Route path="/" exact component={Homepage} />
            <Route path="/about" exact component={Aboutpage} />
            <Route path="/contact" exact component={Contactpage} />
          </Switch>

          <Footer />

        </main>
      </Router>
    </ThemeProvider>
  )
}

export default App
